import java.util.ArrayList;
import java.util.HashMap;

/*
    // Min rewards (AlgoExpert : Hard)
    // Given an input array containing positive distinct integers. You think yourself as a teacher
    and integers are just scores that your students got in their exam. You need to reward then in such a
    way that one who got more score will get more rewards than who got less score, if student 'A' sitting
    next to student 'B' then student 'A' will get more rewards than student 'B'.
    You need to find what will be the minimum number of rewards that you need to distribute.
    Note that every student must get at least one reward.

    Example :   array = [8, 4, 2, 1, 3, 6, 7, 9, 5]
    Answer :    25
    Explanation : 25 is the minimum number of reward that you need to distribute to all your students.
                  If you distribute each student in this way [4, 3, 2, 1, 2, 3, 4, 5, 1] then minimum
                  distributed rewards should be 25.
 */
public class MinRewards {

    // Complexity : Time O(N)  |   Space O(N)       # Naive approach
//    int minRewards (int[] scores) {
//        int[] rewards = new int[scores.length];
//        int totalRewards = 0;
//
//        for (int i=0; i<rewards.length; i++)
//            rewards[i] = 1;
//
//        for (int i=1; i<scores.length; i++) {
//            int j = i - 1;
//            if (scores[i] > scores[j])
//                rewards[i] = rewards[j] + 1;
//            else {
//                while (j>=0 && scores[j]>scores[j+1]) {
//                    rewards[j] = Math.max(rewards[j], rewards[j+1]+1);
//                    j--;
//                }
//            }
//        }
//
//        for (int reward : rewards)
//            totalRewards += reward;
//
//        return totalRewards;
//    }


    // Complexity : Time O(N)  |  Space O(N)    #Peak and Valley approach
//    int minRewards (int[] scores) {
//        int[] rewards = new int[scores.length];
//        int totalRewards = 0;
//
//        for (int i=0; i<rewards.length; i++)
//            rewards[i] = 1;
//
//        ArrayList<Integer> localMinIdxes = getLocalMinIdxes(scores);
//
//        for (int localMinIdx : localMinIdxes) {
//            expandFromLocalMinIdx(localMinIdx, scores, rewards);
//        }
//
//        for (int reward : rewards)
//            totalRewards += reward;
//
//        return totalRewards;
//    }
//
//    void expandFromLocalMinIdx (int localMinIdx, int[] scores, int[] rewards) {
//        int leftIdx = localMinIdx - 1;
//        while (leftIdx>=0 && (scores[leftIdx]>scores[leftIdx+1])) {
//            rewards[leftIdx] = Math.max(rewards[leftIdx], rewards[leftIdx+1]+1);
//            leftIdx--;
//        }
//        int rightIdx = localMinIdx + 1;
//        while (rightIdx<scores.length && (scores[rightIdx]>scores[rightIdx-1])) {
//            rewards[rightIdx] = rewards[rightIdx-1] + 1;
//            rightIdx++;
//        }
//    }
//
//    ArrayList<Integer> getLocalMinIdxes (int[] scores) {
//        ArrayList<Integer> localMinIdxes = new ArrayList<>();
//        if (scores.length == 1) {
//            localMinIdxes.add(0);
//            return localMinIdxes;
//        }
//        if (scores[0] < scores[1])
//            localMinIdxes.add(0);
//        for (int i=1; i<scores.length-1; i++) {
//            if (scores[i-1]>scores[i] && scores[i]<scores[i+1])
//                localMinIdxes.add(i);
//        }
//        if (scores[scores.length-1] < scores[scores.length-2])
//            localMinIdxes.add(scores.length-1);
//        return localMinIdxes;
//    }


    // Complexity : Time O(N)  |   Space O(N)       # Two iteration from left and from right
    int minRewards (int[] scores) {
        int[] rewards = new int[scores.length];
        for (int i=0; i<rewards.length; i++)
            rewards[i] = 1;
        int totalRewards = 0;

        for (int i=1; i<scores.length; i++) {
            if (scores[i] > scores[i-1])
                rewards[i] = rewards[i-1] + 1;
        }

        for (int i=scores.length-2; i>=0; i--) {
            if (scores[i] > scores[i+1])
                rewards[i] = Math.max(rewards[i], rewards[i+1]+1);
        }

        for (int reward : rewards)
            totalRewards += reward;

        return totalRewards;
    }

}
