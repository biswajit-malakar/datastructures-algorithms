import java.util.Arrays;

/*
    // Smallest difference (AlgoExpert : Medium)
    // Given two arrays find one number from each array who are most close to each other or their difference is
    minimum.

    Example : A = [-1, 5, 10, 20, 28, 3]        B = [26, 134, 135, 15, 17]
    Answer: [28, 26]
    Explanation : 28 [from array A] and 26 [from array B] has minimum distance.
 */
public class SmallestDifference {
    // O(N^2) Time   |    O(1) Space
//    int[] smallestDifference (int[] A, int[] B) {
//        int[] ans = new int[2];
//        int smallest_difference = Integer.MAX_VALUE;
//        for (int i=0; i<A.length; i++) {
//            for (int j=0; j<B.length; j++) {
//                int new_difference = Math.abs(A[i]-B[j]);
//                if (new_difference < smallest_difference) {
//                    smallest_difference = new_difference;
//                    ans[0] = A[i];
//                    ans[1] = B[j];
//                }
//            }
//        }
//        return ans;
//    }


    // O(NlogN) Time    |     O(1) Space
    int[] smallestDifference (int[] A, int[] B) {
        int[] valueWithSmallestDifference = new int[2];
        Arrays.sort(A);
        Arrays.sort(B);

        int idxA = 0, idxB = 0;
        int smallest_difference = Integer.MAX_VALUE;

        while (idxA<A.length && idxB<B.length) {
            int new_difference = Math.abs(A[idxA]-B[idxB]);
            if (new_difference < smallest_difference) {
                smallest_difference = new_difference;
                valueWithSmallestDifference[0] = A[idxA];
                valueWithSmallestDifference[1] = B[idxB];
            }
            if (A[idxA] < B[idxB]) idxA++;
            else idxB++;
        }

        return valueWithSmallestDifference;
    }
}
