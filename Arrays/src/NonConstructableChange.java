import java.util.Arrays;

/*
    // Non-constructable changes (AlgoExpert : Easy)
    // Given an array of Integer, each integer is representing coins your task is to find out what will be
    the least value that you can not create using those coins.
    Example : coins = [5, 7, 1, 1, 2, 3, 22]
              Ans : 20  # you can not create worth 20 via taking combination of given coins.
 */
public class NonConstructableChange {
    int nonConstructableChange (int[] coins) {
        Arrays.sort(coins);
        int currentChangeCreated = 0;
        for (int coin : coins) {
            if (coin > currentChangeCreated+1)
                return currentChangeCreated + 1;

            currentChangeCreated += coin;
        }

        return currentChangeCreated + 1;
    }
}
