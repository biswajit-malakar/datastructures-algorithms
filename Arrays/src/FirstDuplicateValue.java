import java.util.HashMap;

/*
    // First duplicate value (AlgoExpert : Medium)
    // Given an integer input array, where 1<=array[i]<=N, where N is length of the input array. Your task
    is to find the duplicate value that occurs first in the input array.

    Example : array = [2, 1, 5, 3, 3, 2, 4]     Answer : 3

    Example : array = [2, 1, 5, 2, 3, 3, 4]      Answer : 2
 */
public class FirstDuplicateValue {

    // Time O(N^2)  |  Space O(1)
//    int firstDuplicateValue (int[] array) {
//        int minimumDuplicateIdx = array.length;
//
//        for (int currIdx=0; currIdx<array.length; currIdx++) {
//            for (int otherIdx=currIdx+1; otherIdx<array.length; otherIdx++) {
//                if (array[currIdx] == array[otherIdx])
//                    minimumDuplicateIdx = Math.min(minimumDuplicateIdx, otherIdx);
//            }
//        }
//
//        if (minimumDuplicateIdx == array.length) return -1;
//        else return array[minimumDuplicateIdx];
//    }


    // Time O(N)  |  Space O(N)
//    int firstDuplicateValue (int[] array) {
//        HashMap<Integer, Integer> visited = new HashMap<>();
//
//        for (int i=0; i< array.length; i++) {
//            if (visited.containsKey(array[i]))
//                return array[i];
//            else
//                visited.put(array[i], array[i]);
//        }
//
//        return -1;
//    }


    // Time O(N)  |  Space O(1)
    int firstDuplicateValue (int[] array) {
        for (int value : array) {
            if (array[Math.abs(value)-1] < 0)
                return Math.abs(value);
            array[Math.abs(value)-1] *= -1;
        }

        return -1;
    }
}
