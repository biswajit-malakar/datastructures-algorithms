import java.util.Arrays;
import java.util.HashMap;

/*
    // Largest Range (AlgoExpert : Hard)
    // Given an array of integers you need to find out the largest range. A range simply means a set of
    numbers that are present anywhere in the input array.

    Example : array = [1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6]
    Largest Range : [0, 7]
    Explanation : The numbers from 0 to 7 inclusive present in the array and the length of that range is much bigger than other ranges.
 */
public class LargestRange {

    // Complexity : Time O(NlogN)  |   Space O(1)
//    int[] largestRange (int[] array) {
//        Arrays.sort(array);
//        int currStart = 0, currEnd = 0, currRange = 0;
//        int ansStart = currStart, ansEnd = currEnd, largestRange = currRange;
//
//        for (int i=1; i<array.length; i++) {
//            if (array[i] == (array[i-1]+1)) {
//                currEnd = i;
//                currRange++;
//            }
//            else {
//                if (currRange > largestRange) {
//                    ansStart = currStart;
//                    ansEnd = currEnd;
//                    largestRange = currRange;
//                }
//                currStart = i;
//                currRange = 0;
//                currEnd = i;
//            }
//        }
//
//        return new int[]{ansStart, ansEnd};
//    }



    // Approach 2 : Time O(N)  |  Space O(N)
    int[] largestRange (int[] array) {
        HashMap<Integer, Boolean> partOfRange = new HashMap<>();

        for (int item : array)
            partOfRange.put(item, false);

        int largestRangeStart = 0, largestRangeEnd = 0, largestRangeLength = 0;

        for (int item : array) {
            if (!partOfRange.get(item)) {
                partOfRange.put(item, true);

                int left = item-1;
                while (partOfRange.containsKey(left) && !partOfRange.get(left)) {
                    partOfRange.put(left, true);
                    left -= 1;
                }
                left += 1;

                int right = item+1;
                while (partOfRange.containsKey(right) && !partOfRange.get(right)) {
                    partOfRange.put(right, true);
                    right += 1;
                }
                right -= 1;

                if (largestRangeLength < (right-left)) {
                    largestRangeLength = (right-left);
                    largestRangeStart = left;
                    largestRangeEnd = right;
                }
            }
        }

        return new int[]{largestRangeStart, largestRangeEnd};
    }
}
