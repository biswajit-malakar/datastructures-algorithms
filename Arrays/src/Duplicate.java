import java.util.Arrays;
import java.util.HashMap;

/*
    // Find duplicate (Leetcode : Medium)
    Given an unsorted integer array in which one number is repeated twice you task is to find that number.

    Example :
    Input: [1, 3, 4, 2, 2]
    Output: 2
 */
public class Duplicate {
    // Time O(N log N)  |   Space O(1)
//    int findDuplicate (int[] array) {
//        Arrays.sort(array);
//        for (int i=0; i<array.length; i++) {
//            if (array[i] == array[i+1])
//                return array[i];
//        }
//        return -1;
//    }


    // Time O(N)  |  Space O(N)
    int findDuplicate (int[] array) {
        HashMap<Integer, Boolean> map = new HashMap<>();
        for (int i=0; i<array.length; i++) {
            if (!map.containsKey(array[i]))
                map.put(array[i], true);
            else
                return array[i];
        }
        return -1;
    }
}








/*
    Algorithm:  # Time O(N) | Space O(N)
    ---------
    findDuplicate (array):
        map = {}
        for i=0...N-1:
            if (array[i] not in map):
                add array[i] in map
            else:
                return array[i]
        return -1   # Represents that array does not have any duplicate value
 */