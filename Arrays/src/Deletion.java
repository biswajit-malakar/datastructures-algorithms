public class Deletion {
    void delete ( int[] array, int k ) {
        if (k == array.length-1)
            array[k] = 0;
        for (int i=k; i<array.length-1; i++)
            array[i] = array[i+1];
        array[array.length-1] = 0;
    }
}









/*
    Algorithm:
    ---------
    delete (array, k):
        if (k == N-1)
            array[k] = 0
        for i=k...N-2:
            array[i] = array[i+1]
        array[N-1] = 0
 */
