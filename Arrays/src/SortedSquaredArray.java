import java.util.Arrays;

/*
    // Sorted squared array (AlgoExpert : Easy)
    // Given a sorted array 'array' your task is to square each element from 'array' have
    to return in another array in sored order.

    Example:
    array = [1, 2, 3, 4, 5, 6]
    Ans : [1, 4, 9, 16, 25, 36]

    OR

    array = [-3, -1, 2, 5, 10]
    Ans : [1, 4, 9, 25, 100]


    #Idea!
        Treat the input array as number line, use this fact that number which is far away from 0
        in both side will produce large squared number.
 */
public class SortedSquaredArray {

    // Approach 1 : O(N log N) Time   |    O(N) Space
//    int[] sortedSquaredArray (int[] array) {
//        int []squaredArray = new int[array.length];
//
//        for (int i=0; i<array.length; i++)
//            squaredArray[i] = array[i]*array[i];
//
//        Arrays.sort(squaredArray);
//        return squaredArray;
//    }


    // Approach 2 : O(N) Time   |   O(N) Space
    int[] sortedSquaredArray (int[] array) {
        int[] squaredArray = new int[array.length];

        int smallerValueIdx = 0, largerValueIdx = array.length-1;                  // two pointers pointing both side of the array
        int currIdxInSquaredArray = squaredArray.length-1;                          // for fill in squaredArray from right to left

        while (smallerValueIdx <= largerValueIdx) {                                // looping until range is invalid
            // find squared value of both number form left and right side
            int squareFromSmallerValueIdx = array[smallerValueIdx]*array[smallerValueIdx];
            int squareFromLargerValueIdx = array[largerValueIdx]*array[largerValueIdx];
            // compare the squared number and put into the array which one is larger
            if (squareFromSmallerValueIdx > squareFromLargerValueIdx) {
                squaredArray[currIdxInSquaredArray] = squareFromSmallerValueIdx;
                currIdxInSquaredArray--;
                smallerValueIdx++;
            }
            else {
                squaredArray[currIdxInSquaredArray] = squareFromLargerValueIdx;
                currIdxInSquaredArray--;
                largerValueIdx--;
            }
        }

        return squaredArray;
    }

}
