/*
    // Array of products (AlgoExpert : Medium)
    // Given an array of size n, input array is unsorted, contains duplicates positive, negative and zero
    as integers and your task is to return another array of size n, where each index of output array
    is product of all elements except element at that index.
    Example :
        array = [5, 1, 4, 2]
        answer : [8, 40, 10, 20]
 */
public class ArrayOfProducts {

    // Time O(N^2)  |  Space O(N)
//    int[] arrayOfProducts (int[] array) {
//        int[] products = new int[array.length];
//
//        for (int currIdx=0; currIdx<array.length; currIdx++) {
//            int product = 1;
//            for (int otherIdx=0; otherIdx<array.length; otherIdx++) {
//                if (otherIdx == currIdx) continue;
//                else product *= array[otherIdx];
//            }
//            products[currIdx] = product;
//        }
//        return products;
//    }


    // Time O(N)  |  Space O(N)
//    int[] arrayOfProducts (int[] array) {
//        int[] products = new int[array.length];
//        int totalProduct = 1;
//
//        for (int currIdx=0; currIdx<array.length; currIdx++)
//            totalProduct *= array[currIdx];
//
//        for (int currIdx=0; currIdx<array.length; currIdx++)
//            products[currIdx] = totalProduct / array[currIdx];
//
//        return products;
//    }


    // Time O(N)  |   Space O(N)
    int[] arrayOfProducts (int[] array) {
        int[] products = new int[array.length];
        int[] productsOfLeftSide = new int[array.length];
        int[] productsOfRightSide = new int[array.length];

        productsOfLeftSide[0] = 1;
        for (int currIdx=1; currIdx<array.length; currIdx++)
            productsOfLeftSide[currIdx] = productsOfLeftSide[currIdx-1] * array[currIdx-1];

        productsOfRightSide[array.length-1] = 1;
        for (int currIdx=array.length-2; currIdx>=0; currIdx--)
            productsOfRightSide[currIdx] = productsOfRightSide[currIdx+1] * array[currIdx+1];

        for (int currIdx=0; currIdx<array.length; currIdx++)
            products[currIdx] = productsOfLeftSide[currIdx] * productsOfRightSide[currIdx];

        return products;
    }
}




/*
    Algorithm :
    ---------
    arrayOfProducts (input):
        left, right = [], []
        products = []

        left[0] = 1, right[0] = 1

        for i=1...N-1:
            left[i] = left[i-1] * input[i-1]

        for i=N-2...0:
            right[i] = right[i+1] * input[i+1]

        for i=0...N-1:
            products[i] = left[i] * right[i]

        return products
 */