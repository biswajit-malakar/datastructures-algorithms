/*
    // SubArray sort (AlgoExpert : Hard)
    // Given an unsorted input array of size at least 2, your task is to find a sub-array from anywhere
    in the input array that makes the input array unsorted (means if you sort that sub-array your entire
    array will be sorted) and you have to return the starting and ending index of that sub-array in a form
    of array of size 2.

    Example : array = [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]
    Answer : [3, 9]
    Explanation : the sub-array from 3 to 9 makes the whole array unsorted.
 */
public class SubArraySort {
    int[] subarraySort (int[] array) {
        int minOutOrder = Integer.MAX_VALUE, maxOutOrder = Integer.MIN_VALUE;

        for (int i=0; i<array.length; i++) {
            if (isOutOfOrder(array, i)) {
                minOutOrder = Math.min(minOutOrder, array[i]);
                maxOutOrder = Math.max(maxOutOrder, array[i]);
            }
        }

        if (minOutOrder == Integer.MAX_VALUE)
            return new int[]{-1, -1};

        int subarrayLeftIdx = 0;
        while (minOutOrder >= array[subarrayLeftIdx])
            subarrayLeftIdx++;

        int subarrayRightIdx = array.length-1;
        while (maxOutOrder <= array[subarrayRightIdx])
            subarrayRightIdx--;

        return new int[]{subarrayLeftIdx, subarrayRightIdx};
    }

    boolean isOutOfOrder (int[] array, int idx) {
        if (idx == 0)
            return array[idx] > array[idx+1];
        else if (idx == array.length-1)
            return array[idx] < array[idx-1];
        else
            return array[idx-1]>array[idx] || array[idx]>array[idx+1];
    }
}
