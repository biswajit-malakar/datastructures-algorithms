import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/*
    // Three number sum (AlgoExpert : Medium)
    // Given a distinct array of integer and an integer number, pick any three distinct number from given array
    and check whether their sum is equal with provided integer or not.
    Example : array = [12, 3, 1, 2, -6, 5, -8, 6]     targetSum = 0     Answer : True

    [-8, -6, 1, 2, 3, 5, 6, 12]

 */
public class ThreeNumberSum {

    // Approach 1 : O(N^2) Time   |    O(N) Space
//    boolean threeNumberSum (int[] array, int targetSum) {
//        HashMap<Integer, Integer> map = new HashMap<>();
//
//        for (int i=0; i<array.length; i++) {
//            for (int j=i+1; j<array.length; j++) {
//                int key = targetSum - (array[i]+array[j]);
//                if (map.containsKey(key)) return true;
//            }
//            map.put(array[i], array[i]);
//        }
//
//        return false;
//    }


    // Approach 2 : O(N^2) Time   |    O(1) Space
    boolean threeNumberSum (int[] array, int targetSum) {
        Arrays.sort(array);
        int left, right;

        for (int i=0; i<array.length-2; i++) {
            left = i+1;
            right = array.length-1;
            while (left < right) {
                int currentSum = array[i] + array[left] + array[right];
                if (currentSum == targetSum) return true;
                else if (currentSum < targetSum) left++;
                else right--;
            }
        }

        return false;
    }


    // Approach 2 (Question Modified) : Let's say you need to return a list of triplets.
    List<List<Integer>> threeNumberSumTriplets (int[] array, int targetSum) {
        Arrays.sort(array);
        int left, right;
        List<List<Integer>> triplets = new ArrayList<>();

        for (int i=0; i<array.length-2; i++) {
            left = i+1;
            right = array.length-1;
            while (left < right) {
                int currentSum = array[i] + array[left] + array[right];
                if (currentSum == targetSum) {
                    triplets.add(Arrays.asList(array[i], array[left], array[right]));
                    left++;
                    right--;
                }
                else if (currentSum < targetSum) left++;
                else right--;
            }
        }

        return triplets;
    }
}
