/*
    // Validate Subsequence (AlgoExpert : Easy)
    // Given two arrays 'array1' and 'array2' your task is to check is 'array2' is a subsequence of 'array1' or not
    subsequence means that all element of 'array2' will appear in 'array1' and also they will be in same
    appearing order.

    array1 = [5, 1, 22, 25, 6, -1, 8 10]      array2 = [1, 6, -1, 10]
    Ans : True
 */
public class ValidateSubsequence {

    // Approach 1 : O(N) Time   |   O(1) Space
    boolean validateSubsequence (int[] array, int[] sequence) {
        int seqIdx = 0;
        for (int value : array) {
            if (seqIdx == sequence.length) break;
            if (sequence[seqIdx] == value) seqIdx++;
        }
        return seqIdx == sequence.length;
    }
}
