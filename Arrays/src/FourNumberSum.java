import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/*
    // Four number sum (AlgoExpert : Hard)
    // Given an input array and an integer k, your task is to find any group of four numbers from input array
    whose sum is equal with k, and return all of those groups in a form of list.

    Example :   array = [7, 6, 4, -1, 1, 2]  ,  k = 16
    Output : [ [7, 6, 4, -1], [7, 6, 1, 2] ]
 */
public class FourNumberSum {

    List<List<Integer>> fourNumberSum (int[] array, int targetSum) {
        List<List<Integer>> quadruples = new LinkedList<>();    // Quadruplets, for storing answer.
        HashMap<Integer, List<List<Integer>>> allPairSum = new HashMap<>();     // HashMap, for storing visited pairs with their sum

        for (int currIdx=1; currIdx<array.length-1; currIdx++) {    // go through each element in range of 2nd to 2nd last
            for (int otherIdx=currIdx+1; otherIdx<array.length; otherIdx++) {   // go through all elements those are on right side
                int currentPairSum = array[currIdx] + array[otherIdx];      // get current pair sum
                int requiredPairSum = targetSum - currentPairSum;   // get the required pair sum
                if (allPairSum.containsKey(requiredPairSum)) {      // if required pair sum is in visited pairs
                    for (List<Integer> pair : allPairSum.get(requiredPairSum)) {    // go through all pairs
                        quadruples.add(Arrays.asList(pair.get(0), pair.get(1), array[currIdx], array[otherIdx]));   // store all elements from current pair and visited pair in quadruplets as we found answer.
                    }
                }
            }
            for (int otherIdx=0; otherIdx<currIdx; otherIdx++) {    // go through all elements those are on left side of current index.
                int currentPairSum = array[currIdx] + array[otherIdx];      // store current pair sum to mark as visited
                if (!allPairSum.containsKey(currentPairSum)) {      // if current pair sum is not in visited pairs
                    List<List<Integer>> tempList = new LinkedList<>();      // create a new list for temporary use
                    tempList.add(Arrays.asList(array[currIdx], array[otherIdx]));   // add current number pairs
                    allPairSum.put(currentPairSum, tempList);   // mark that pair as visited along with the sum
                }
                else {      // if current pair sum is already marked as visited
                    List<List<Integer>> tempList = allPairSum.get(currentPairSum);  // get the visited pairs from hashmap
                    tempList.add(Arrays.asList(array[currIdx], array[otherIdx]));   // append a new pair that we just got
                    allPairSum.put(currentPairSum, tempList);   // update the visited pairs list
                }
            }
        }

        return quadruples;      // return quadruplets that contains all quadruplet whose sum is targetSum
    }
}
