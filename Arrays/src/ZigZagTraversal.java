import java.util.ArrayList;

/*
    // ZigZag Traversal (AlgoExpert : Hard)
    // Given a 2D array of type integer. Your task is to travel through the array in zigzag path and return
    a 1D array containing all elements of input array in zigzag order.

    Example :   1  3  4  10
                2  5  9  11
                6  8  12 15
                7  13 14 16

     output : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 15, 16]
 */
public class ZigZagTraversal {
//    int[] zigzagTraversal (int[][] array) {
//        int[] visited = new int[array.length*array[0].length];
//        boolean direction = false;
//        int row = 0, col = 0;
//        int rN = array.length-1, cN = array[0].length-1;
//        int k = 0;
//
//        while (row<=rN && col<=cN) {
//            visited[k++] = array[row][col];
//
//            if (col==cN /*&& row<rN*/ && direction) {       // right-border
//                row++;
//                direction = false;
//            }
//            else if (row==0 /*&& col<cN*/ && direction) {    // top-border
//                col++;
//                direction = false;
//            }
//            else if (col==0 /*&& row<rN*/ && !direction) {      // left-border
//                row++;
//                direction = true;
//            }
//            else if (row==rN /*&& col<cN*/ && !direction) {       // bottom-border
//                col++;
//                direction = true;
//            }
//            else {
//                if (direction) {
//                    row--;
//                    col++;
//                }
//                else {
//                    row++;
//                    col--;
//                }
//            }
//
//        }
//
//        return visited;
//    }




    // Complexity :  Time O(N)  |   Space O(N)
    ArrayList<Integer> zigzagTraversal (int[][] array) {
        ArrayList<Integer> visited  = new ArrayList<>();
        int height = array.length-1;
        int width = array[0].length-1;
        int row = 0, col = 0;
        boolean goingDown = true;

        while (!isOutOfBound(row, col, height, width)) {
            visited.add(array[row][col]);
            if (goingDown) {
                if (col==0 || row==height) {
                    goingDown = false;
                    if (row == height) col += 1;
                    else row += 1;
                }
                else {
                    row += 1;
                    col -= 1;
                }
            }
            else {
                if (row==0 || col==width) {
                    goingDown = true;
                    if (col == width) row += 1;
                    else col += 1;
                }
                else {
                    row -= 1;
                    col += 1;
                }
            }
        }

        return visited;
    }

    boolean isOutOfBound (int row, int col, int height, int width) {
        return row<0 || row>height || col<0 || col>width;
    }
}
