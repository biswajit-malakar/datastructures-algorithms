/*
    // Move element to end (AlgoExpert : Medium)
    // Given an array of Integers and an integer number, your task is to all instances present in input array
    to the end and order of other elements does not matter. Do this in-place.

    Example : Array = [2, 1, 2, 2, 2, 3, 4, 2]      moveAll = 2
    Answer : [1, 4, 3, 2, 2, 2, 2, 2]
 */
public class MoveElementToEnd {

    // O(N) Time   |    O(1) Space
    void moveElementToEnd (int[] array, int toMove) {
        int currIdx = 0, movedIdx = array.length-1;
        while (currIdx < movedIdx) {
            if (array[currIdx] == toMove) {
                int temp = array[currIdx];
                array[currIdx] = array[movedIdx];
                array[movedIdx] = temp;

                movedIdx--;
            }
            else currIdx++;
        }
    }
}
