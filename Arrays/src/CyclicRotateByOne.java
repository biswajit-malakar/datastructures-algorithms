/*

 */
public class CyclicRotateByOne {
    void rotate ( int[] array ) {
        int lastValue = array[array.length-1];
        for (int i=array.length-2; i>=0; i--)
            array[i+1] = array[i];
        array[0] = lastValue;
    }

}






/*
    Algorithm:
    ---------
    cyclicRotate (array):
        lastValue = array[N-1]
        for i=N-2...0:
            array[i+1] = array[i]
        array[0] = lastValue
 */