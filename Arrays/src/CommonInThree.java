import java.util.ArrayList;

/*
    // Common in three (Leetcode : Easy)
    You have given three sorted integer arrays, your task is to find all the elements that are in
    common in all three of them.

    Example :
        Inputs:
        int[] A = {1,3,5,7,9};
        int[] B = {0,1,2,4,8,9,10};
        int[] C = {1,2,3,4,5,6,7,8,9,10};

        Output:
        {1,9}
 */
public class CommonInThree {

    /*
    Time Complexity : O(N)
    Space Complexity : O(1)
    Arrays are needs to be sorted in ascending order.
     */
    ArrayList<Integer> commonInThree (int[] A, int[] B, int[] C) {
        ArrayList<Integer> common = new ArrayList<>();
        int a=0, b=0, c=0;
        while (a<A.length && b<B.length && c<C.length) {
            if (A[a]==B[b] && A[a]==C[c]) {
                common.add(A[a]);
                a++;
                b++;
                c++;
            }
            else if (A[a]<B[b] && A[a]<C[c])
                a+=1;
            else if (B[b]<A[a] && B[b]<C[c])
                b+=1;
            else
                c+=1;
        }
        return common;
    }
}





/*
    Algorithm:  Time O(N)  |  Space O(C)    C: # common elements
    ----------
    commonInThree (A, B, C):
        common = []
        a=0, b=0, c=0
        while ( a<len(A) && b<len(B) && c<len(C) ):
            if (A[a] == B[b] == C[c]):
                common.append(A[a])
                a+=1, b+=1, c+=1
            else if (A[a]<B[b] && A[a]<C[c]):
                a+=1
            else if (B[b]<A[a] && B[b]<C[c]):
                b+=1
            else:
                c+=1
        return common
 */