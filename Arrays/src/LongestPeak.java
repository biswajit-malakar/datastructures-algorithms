/*
    // Longest peak (AlgoExpert : Medium)
    // Given an array you need to find the longest peak.

    Example:
        array = [1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3]
        Answer : 6
        Explanation : The peak (10) has maximum number of elements that are strictly less than (10) in either side
                        { 0, 10, 6, 5, -1, -3 }
 */
public class LongestPeak {

    // Complexity : Time O(N) | Space O(P) P is # peaks.
    int longestPeak (int[] array) {
        int longestPeakLength = 0;

        int idx = 1;
        while (idx < array.length-1) {      // iterate over 2nd index to 2nd last index
            if (array[idx-1]<array[idx] && array[idx]>array[idx+1]) {   // if current index contains a peak
                int leftIdx = idx-1;        // to get the left length from the peak
                while (leftIdx>0 && array[leftIdx-1]<array[leftIdx]) leftIdx -= 1;

                int rightIdx = idx+1;       // to get the right length from the peak
                while (rightIdx<array.length-1 && array[rightIdx+1]<array[rightIdx]) rightIdx += 1;

                int currPeakLength = rightIdx - leftIdx + 1;    // get the width of that peak
                longestPeakLength = Math.max(currPeakLength, longestPeakLength);    // getting the maximum width / distance

                idx = rightIdx + 1;     // skip all the minimum elements that are belong to the peak
            }
            else idx++;     // if current index does not contain a peak go to the next index
        }

        return longestPeakLength;
    }
}
