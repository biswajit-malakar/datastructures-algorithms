/*
    // Monotonic array (AlgoExpert : Medium)
    // Given an array your task is to check is the array is monotonic or not. Monotonic means all numbers of
    that array is entirely non-increasing or entirely non-decreasing and array also can contain same numbers.

    Example : array = [-1, -5, -10, -1100, -1100, -1101, -1102, -9001]
    Answer : True
    Explanation : all numbers are going downward, it's okay to have same numbers.
 */
public class MonotonicArray {
    boolean isMonotonic (int[] array) {
        if (isIncreasing(array)) {
            for (int i=0; i<array.length-1; i++) {
                if (array[i] > array[i+1]) return false;
            }
        }
        else {
            for (int i=0; i<array.length-1; i++) {
                if (array[i] < array[i+1]) return false;
            }
        }
        return true;
    }

    boolean isIncreasing (int[] array) {
        if (array[0] < array[array.length-1]) return true;
        return false;
    }
}
