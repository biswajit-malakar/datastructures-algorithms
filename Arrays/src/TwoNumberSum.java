import java.util.Arrays;
import java.util.HashMap;

/*
    // Two number sum (AlgoExpert : Easy)
    // Given one distinct unsorted array 'array  and an integer number 'target' your task is to grab
    any two numbers form the array and sum them up if the sum is equal with target return true otherwise false.

    array = [3, 5, -4, 8, 11, 1, -1, 6]    target = 10
    Ans : True [ 11 + (-1) ] == 10
 */
public class TwoNumberSum {

    // Approach 1 : O(N^2) Time  |  O(1) Space
//    boolean twoNumberSum (int[] array, int target) {
//        if (array.length == 0) return false;
//        for (int i=0; i<array.length; i++) {
//            for (int j=i+1; j<array.length; j++) {
//                int sum = array[i] + array[j];
//                if (sum == target) return true;
//            }
//        }
//        return false;
//    }


    // Approach 2 : O(N log N) Time   |  O(1) Space  **Assuming doing quick sort or other in-place sort
//    boolean twoNumberSum (int[] array, int target) {
//        if (array.length == 0) return false;
//        Arrays.sort(array);
//        int left = 0, right = array.length-1;
//        while (left < right) {
//            int sum = array[left] + array[right];
//            if (sum == target) return true;
//            else if (sum < target) left++;
//            else right--;
//        }
//        return false;
//    }


    // Approach 3 : O(N) Time  |  O(N) Space
    // Version 1
//    boolean twoNumberSum (int[] array, int target) {
//        if (array.length == 0) return false;
//
//        HashMap<Integer, Integer> map = new HashMap<>();
//        for (int i=0; i<array.length; i++) {
//            map.put(array[i], array[i]);
//        }
//
//        for (int i=0; i<array.length; i++) {
//            int y = target - array[i];
//            if (map.containsKey(y)) return true;
//        }
//
//        return false;
//    }

    // Version 2
    boolean twoNumberSum (int[] array, int target) {
        if (array.length == 0) return false;

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i=0; i<array.length; i++) {
            int y = target - array[i];
            if (map.containsKey(y)) return true;
            map.put(array[i], array[i]);
        }

        return false;
    }
}