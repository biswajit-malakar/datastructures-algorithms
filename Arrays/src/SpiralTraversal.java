import java.util.ArrayList;

/*
    // Spiral Traversal (AlgoExpert : Medium)
    // Given a 2D array of size (N*M) as input your task is to travel on that 2D array in spiral shape
    and return all visited element in a 1D array as output.

    Example : 2D Array = [1, 2, 3, 4
                          5, 6, 7, 8
                          9, 10, 11, 12];

    Answer : 1D Array = [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7]
 */
public class SpiralTraversal {

    ArrayList<Integer> spiralTraversal (int[][] array) {
        ArrayList<Integer> output = new ArrayList<>();

        int startRow = 0, endRow = array.length-1;
        int startCol = 0, endCol = array[0].length-1;

        while (startRow<=endRow && startCol<=endCol) {
            for (int col=startCol; col<=endCol; col++) output.add(array[startRow][col]);
            for (int row=startRow+1; row<=endRow; row++) output.add(array[row][endCol]);
            for (int col=endCol-1; col>=startCol; col--) output.add(array[endRow][col]);
            for (int row=endRow-1; row>startRow; row--) output.add(array[row][startCol]);

            startRow++;
            endRow--;
            startCol++;
            endCol--;
        }

        return output;
    }
}
