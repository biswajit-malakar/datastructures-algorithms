import java.util.HashMap;

/*
    // Tournament winner (AlgoExpert : Easy)
    // Given an 2D array of type string 'competition' and a 'result' array of type integer.
    // '1' is 'result' array represent 1st team from 'competition' and '0' in 'result' array represent 2nd
    temp from 'competition'. Return the name of the team who got maximum points.

    Example :
    competition = [
        ["HTML", "C#"],
        ["C#", 'Python"],
        ["Python", "HTML"]
    ]
    result = [0, 0, 1]          # length of result and competition array will be same.

    Winner is "Python"
 */
public class TournamentWinner {
    String tournamentWinner (String[][] competition, int[] result) {
        HashMap<String, Integer> scores = new HashMap<>();

        for (int i=0; i< competition.length; i++) {
            if (result[i] == 0) {
                if (scores.containsKey(competition[i][1]))
                    scores.put(competition[i][1], scores.get(competition[i][1])+1);
                else
                    scores.put(competition[i][1], 1);
            }
            else {
                if (scores.containsKey(competition[i][0]))
                    scores.put(competition[i][0], scores.get(competition[i][0])+1);
                else
                    scores.put(competition[i][0], 1);
            }
        }

        int winScore = 0;
        String winner = "";
        for (String key : scores.keySet()) {
            if (winScore < scores.get(key)) {
                winScore = scores.get(key);
                winner = key;
            }
        }
        return winner;
    }
}
