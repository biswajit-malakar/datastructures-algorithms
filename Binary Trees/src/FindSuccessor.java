import java.util.HashMap;

/*
    // Find successor (AlgoExpert : Medium)
    // In in-order traversal a successor of a node comes before come after that node.
                            1
                         /     \
                        2       3
                     /    \
                    4      5
                  /
                 6
                 Successor of node (5) is (1)
    #Idea!
    Case #1 : if (node.right is present) : first node from right subtree is successor.
    Case #2 : if (node.right is not present) :
                    Augmenting a HashMap for storing parent of a node.
                    Keep going upper level using HashMap link until you part of your parent's left subtree.
 */
public class FindSuccessor {
    HashMap<Node, Node> parent = new HashMap<>();
    void augmenting (Node root, HashMap<Node, Node> parent) {
        if (root==null || (root.left==null && root.right==null)) return;
        if (root.left != null) parent.put(root.left, root);
        if (root.right != null) parent.put(root.right, root);
        augmenting(root.left, parent);
        augmenting(root.right, parent);
    }

    Node successor (Node root) {
        if (root == null) return null;
        if (root.right != null) return first_node (root.right);
        else {
            while (parent.get(root)!=null && parent.get(root).right==root) root = parent.get(root);
            return parent.get(root);
        }
    }

    Node first_node (Node node) {
        while (node.left != null) node = node.left;
        return node;
    }
}
