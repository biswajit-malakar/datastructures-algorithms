import java.util.ArrayList;
import java.util.HashMap;
/*
    // Find nodes at distance 'k' (AlgoExpert : Hard)
    Given the root of a binary tree, the value of a target node target, and an integer k,
    return an array of the values of all nodes that have a distance k from the target node.
    You can return the answer in any order.

    Input: root = [3,5,1,6,2,0,8,null,null,7,4], target = 5, k = 2
    Output: [7,4,1]
    Explanation: The nodes that are a distance 2 from the target node (with value 5) have values 7, 4, and 1.

    Input: root = [1], target = 1, k = 3
    Output: []

    #idea!
    Augment a tree using HashMap with node with parent.
    Travel from target node and go 'k' distance 3 directions left, right, parent.
 */
public class AllNodesDistanceK {
    ArrayList<Integer> distanceK (Node root, Node target, int k) {
        HashMap<Node, Node> parent = new HashMap<>();
        parent.put(root, null);
        HashMap<Node, Boolean> visited = new HashMap<>();
        ArrayList<Integer> list = new ArrayList<>();
        augmenting(root, parent);
        find_nodes(target, k, visited, list, parent);
        return list;
    }

    // Augment parent HashMap so that it can act like parent pointer.
    void augmenting (Node root, HashMap<Node, Node> parent) {
        if (root == null || (root.left==null && root.right==null)) return;
        if (root.left != null) parent.put(root.left, root);
        if (root.right != null) parent.put(root.right, root);
        augmenting(root.left, parent);
        augmenting(root.right, parent);
    }

    // find all nodes that are ar 'k' distance away from the target node.
    void find_nodes (Node root, int k, HashMap<Node, Boolean> visited, ArrayList<Integer> list, HashMap<Node, Node> parent) {
        if (root == null) return;
        if (visited.containsKey(root)) return;
        if (k == 0) { list.add(root.item); return; }
        visited.put(root, true);
        find_nodes(root.left, k-1, visited, list, parent); // go to left direction
        find_nodes(root.right, k-1, visited, list, parent); // go to right direction
        find_nodes(parent.get(root), k-1, visited, list, parent); // go to parent direction
    }
}
