import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
class BinaryTreesBasics {
    public static void main (String[] args) {
        int[] tree = {1,2,4,8,-1,-1,9,-1,-1,5,10,-1,-1,-1,3,6,-1,-1,7,-1,-1};
        Tree_Operations o = new Tree_Operations();
        Node A = o.buildTree(tree);
        System.out.println(o.max_path_sum(A));
    } 
}


class Node {
    int item;
    Node parent;
    Node left;
    Node right;
    Node (int item) {
        this.item = item;
        parent = null;
        left = null;
        right = null;
    }
}


class Tree_Operations {
    void inorder_traversal (Node Root) {
        if (Root == null) return;
        inorder_traversal(Root.left);
        System.out.print(Root.item + "-> ");
        inorder_traversal(Root.right);
    }

    Node subtree_first (Node A) {
        if (A.left == null) return A;
        return subtree_first(A.left); 
    }

    Node subtree_last (Node A) {
        if (A.right == null) return A;
        return subtree_last(A.right);
    }

    Node successor (Node A) {
        if (A.right != null) return subtree_first(A.right);
        while (A.parent!=null && A.parent.right==A) A = A.parent;
        return A.parent;
    }

    Node predecessor (Node A) {
        if (A.left != null) return subtree_last(A.left);
        while (A.parent!=null && A.parent.left==A) A = A.parent;
        return A.parent;
    }

    // insert 'X' before 'A'
    void insert_before (Node A, Node X) {
        if (A.left == null) {
            A.left = X;
            X.parent = A;
        }
        else {
            A = subtree_last(A.left);
            A.right = X;
            X.parent = A;
        }
    }

    // insert 'X' after 'A'
    void insert_after (Node A, Node X) {
        if (A.right == null) {
            A.right = X;
            X.parent = A;
        }
        else {
            A = subtree_first(A.right);
            A.left = X;
            X.parent = A;
        }
    }

    // delete node 'A'
    void subtree_delete (Node A) {
        if (A.left==null && A.right==null) {
            if (A.parent == null) A = null;
            else {
                if (A.parent.left == A) A.parent.left = null;
                else A.parent.right = null;
            }
        }
        else {
            Node B;
            if (A.left != null) B = subtree_last(A.left);
            else B = subtree_first(A.right);
            int temp = A.item;
            A.item = B.item;
            B.item = temp;
            subtree_delete(B);
        }
    }

    Node subtree_find (Node Root, int item) {
        if (Root == null) return Root;
        subtree_find(Root.left, item);
        if (Root.item == item) return Root;
        return subtree_find(Root.right, item);
    }


    //Question 1: // build a binary tree with a given array, pre-order 
    int i = -1;
    Node buildTree (int[] nodes) {
        i++;
        if (nodes[i]==-1) return null;
        Node A = new Node (nodes[i]);
        A.left = buildTree(nodes);
        A.right = buildTree(nodes);
        return A;
    }

    //Question 2: // level-order traversal (list based implementation)
    void level_order_traversal_ArrayList (Node A) {
        if (A==null) return;
        ArrayList<Node> frontier = new ArrayList<>();
        frontier.add(A);
        while (!frontier.isEmpty()) {
            ArrayList<Node> next = new ArrayList<>();
            for (Node node : frontier) {
                System.out.print(node.item+" ");
                if (node.left!=null) next.add(node.left);
                if (node.right!=null) next.add(node.right);
            }
            frontier = next;
            System.out.println();
        } 
    }

    //Question 3: // level-order-traversal (queue based implementation)
    void level_order_traversal_Queue (Node A) {
        if (A==null) return;
        Queue<Node> queue = new LinkedList<>();
        queue.add(A);
        queue.add(null);
        while (!queue.isEmpty()) {
            Node node = queue.remove();
            if (node==null) {
                System.out.println();
                if (queue.isEmpty()) break;
                else queue.add(null);
            }
            else {
                System.out.print(node.item+" ");
                if (node.left!=null) queue.add(node.left);
                if (node.right!=null) queue.add(node.right);
            }
        }
    }

    //Question 4: // return total number of nodes in a tree
    int countOfNodes (Node A) {
        if (A == null) return 0;
        return countOfNodes(A.left) + countOfNodes(A.right) + 1;
    }

    //Question 5: // return sum of nodes in a tree
    int sumOfNodes (Node A) {
        if (A == null) return 0;
        return sumOfNodes(A.left) + sumOfNodes(A.right) + A.item;
    }

    //Question 6: // return height of a node
    int heightOfNode (Node A) {
        if (A == null) return 0;
        return Math.max(heightOfNode(A.left), heightOfNode(A.right)) + 1;
    }
 
    //Question 7: // return diameter of a tree (diameter means longest path between two nodes, path may or may not go via root)
                  // longest path means maximum number of edges in the path.
    static int ans = -1;
    int diameter (Node A) {
        if (A == null) return 0;
        int lh = diameter(A.left);
        int rh = diameter(A.right);
        ans = Math.max (ans, lh+rh);
        return Math.max (lh, rh)+1;
    }
    int diameterOfTree (Node A) {
        diameter(A);
        return ans;
    }

    // Question 8: // Subtree of another tree
    boolean find_subtree (Node s2, Node s1) {
        if (s2 == null) return false;
        boolean b1 = false;
        if (s2.item == s1.item) b1 = find_subtree_helper (s2, s1);
        boolean b2 = find_subtree(s2.left, s1);
        boolean b3 = find_subtree(s2.right, s1);
        return b1||b2||b3;
    }

    boolean find_subtree_helper (Node s2, Node s1) {
        if (s2==null && s1==null) return true;
        if (s2!=null && s1==null) return true;
        if (s2==null && s1!=null) return false;
        if (s2.item == s1.item) {
            boolean b1 = find_subtree_helper(s2.left, s1.left);
            boolean b2 = find_subtree_helper(s2.right, s1.right);
            return b1&&b2;
        }
        else return false;
    }

    // Question 9: // Branch Sums
    ArrayList<Integer> branch_sums (Node A) {
        ArrayList<Integer> ans = new ArrayList<>();
        branch_sums_helper (A, 0, ans);
        return ans;
    }
    void branch_sums_helper (Node A, int sum, ArrayList<Integer> ans) {
        if (A==null) return;
        if (A.left==null && A.right==null) {
            ans.add(sum+A.item);
            return;
        }
        branch_sums_helper(A.left, sum+A.item, ans);
        branch_sums_helper(A.right, sum+A.item, ans);
    }

    // Question 10: // find maximum depth of a tree
    int max_depth (Node A) {
        if (A == null) return 0;
        int left = max_depth(A.left);
        int right = max_depth(A.right);
        return 1 + Math.max (left, right);
    }

    // Question 11: // invert binary tree (iterative solution)
    // Node invert_tree (Node A) {
    //     ArrayList<Node> frontier = new ArrayList<>(); frontier.add(A);
    //     while (!frontier.isEmpty()) {
    //         ArrayList<Node> next = new ArrayList<>();
    //         for (Node node : frontier) {
    //             if (node==null) break;
    //             Node left=null, right=null;
    //             if (node.left != null) {
    //                 next.add(node.left);
    //                 left = node.left;
    //             }
    //             if (node.right != null) {
    //                 next.add(node.right);
    //                 right = node.right;
    //             }
    //             node.left = right;
    //             node.right = left;
    //         }
    //         frontier = next;
    //     }
    //     return A;
    // }

    // Question 11: // invert binary tree (recursive solution)
    Node invert_tree (Node A) {
        if (A==null) return A;
        if (A.left==null && A.right==null) return A;

        Node temp = A.left;
        A.left = A.right;
        A.right = temp;

        invert_tree(A.left);
        invert_tree(A.right);
        return A;
    }

    // Question 12: // Height balanced binary tree
    HashMap<Node, Integer> heights = new HashMap<>();
    boolean balanced_or_not (Node A) {
        if (A == null) return true;
        return balanced_or_not(A.left) && balanced_or_not(A.right) && (skew(A)<2&&skew(A)>-2);
    }

    int height (Node A) {
        if (A == null) return -1;
        if (heights.containsKey(A)) return heights.get(A);
        int left_height = height (A.left);
        int right_height = height (A.right);
        int h = 1 + Math.max (left_height, right_height);
        heights.put(A, h);
        return h;
    }

    int skew (Node A) {
        return height(A.right) - height(A.left);
    }

    // Question 13: // Max path sum in binary tree
    int max_path_sum_helper (Node A, int curr_sum, int max_sum) {
        if (A == null) return max_sum;
        if (curr_sum < curr_sum+A.item) curr_sum += A.item;
        max_sum = Math.max (curr_sum, max_sum);
        int left_sum = max_path_sum_helper (A.left, curr_sum, max_sum);
        int right_sum = max_path_sum_helper (A.right, curr_sum, max_sum);
        return Math.max(left_sum, right_sum);
    }

    int max_path_sum (Node A) {
        return max_path_sum_helper(A, 0, 0);
    }
}