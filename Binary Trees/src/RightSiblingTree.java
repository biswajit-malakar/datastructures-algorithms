/*
    # Right Sibling Tree (AlgoExpert : Very Hard) . Mutate existing tree DO NOT create new tree
                     1                                        1
                 /       \                                   /
               2          3                                 2 --------- 3
             /  \       /  \                               /           /
            4    5     6    7       ==>>                  4 --- 5 --- 6 --- 7
          / \     \   /    / \                           /           /     /
         8   9    10 11   12 13                         8 - 9 10 - 11    12 - 13
                     /                                             /
                    14                                            14

     # Pattern 1 : if 'Node' is leftChild of 'Parent' then 'Node.right' points to 'Parent.right'. Only if 'Parent' is present.
     # Pattern 2 : if 'Node' is rightChild of 'Parent' then 'Node.right' points to 'Parent.right.left'. Only if 'Parent & Parent.right' is present.
     # Store 'Left' & 'Right' node before doing anything otherwise you lost connections
     # Do In-Order Traversal
 */
public class RightSiblingTree {
    Node right_sibling_tree (Node root) {
        mutate (root, null, false);
        return root;
    }

    void mutate (Node node, Node parent, boolean isLeftChild) {
        if (node == null) return;                               // Base Case
        Node left = node.left, right = node.right;              // Copying nodes before doing operations
        mutate (left, node, true);                     // Go to left side of tree
        if (parent == null) node.right = null;                 // if 'parent' null make 'node.right' null. applicable for the 'root' node
        else if (isLeftChild) node.right = parent.right;       // if 'Left' of 'Parent' make 'Node.right' as 'Parent.right'. #Pattern 1
        else {                                                 // if 'Right' of 'Parent' make 'Node.right' as 'Parent.right.left'. #Pattern 2
            if (parent.right == null) node.right = null;
            else node.right = parent.right.left;
        }
        mutate(right, node, false);                   // Go to right side of tree
    }
}
