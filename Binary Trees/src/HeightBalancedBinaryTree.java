import java.util.HashMap;

/*
    // Height balanced binary tree (AlgoExpert : Medium)
    // Given a binary tree and check if the tree is height balanced or not.
                            1
                        /       \
                       2         3
                     /   \     /
                    4     5   6
                        /  \
                       7    8
                       Answer : True
 */
public class HeightBalancedBinaryTree {
    HashMap<Node, Integer> heights = new HashMap<>(); // store heights of all calculated nodes.
    // get height of a node
    int height (Node node) {
        if (node == null) return -1;
        return heights.get(node);
    }

    // difference between right subtree height and left subtree height.
    int skew (Node node) {
        return height(node.right) - height(node.left);
    }

    boolean isBalanced (Node root) {
        if (root == null) return true;
        if (root.left==null && root.right==null) {
            heights.put(root, 0);
            return true;
        }
        boolean b1 = isBalanced(root.left);
        boolean b2 = isBalanced(root.right);
        boolean b3 = false;
        int h = Math.max(height(root.left), height(root.right)) + 1;
        heights.put(root, h);
        if (-2<skew(root) && 2>skew(root)) b3 = true;
        return b1 && b2 && b3;
    }
}
