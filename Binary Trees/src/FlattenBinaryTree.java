import java.util.LinkedList;
import java.util.Queue;
/*
    // Flatten a Binary Tree (AlgoExpert : Very Hard)
    // Given a binary tree, you have to flat a binary tree into linked list like tree. You can not create
       new tree, you have to mutate the existing tree.
                    1                               1
                /       \                            \
               2         5       ==>>                 2
             /   \                                     \
            3     4                                     3
                                                         \
                                                          4
                                                           \
                                                            5
         # Idea!
         Do pre-order travel into the tree and store nodes into a queue.
         Remove nodes from queue one by one and add each of them into the right side and make left as null.
 */
public class FlattenBinaryTree {
    // populate the queue in pre-order
    void insert_into_queue (Node root, Queue<Integer> queue) {
        if (root == null) return;
        queue.add(root.item);
        insert_into_queue(root.left, queue);
        insert_into_queue(root.right, queue);
    }

    void flatten_binary_tree (Node root) {
        if (root == null) return;
        Queue<Integer> queue = new LinkedList<>();
        insert_into_queue(root, queue);
        while (!queue.isEmpty()) {
            root.item = queue.remove();
            root.left = null;
            if (root.right==null && !queue.isEmpty()) root.right = new Node();
            root = root.right;
        }
    }
}
