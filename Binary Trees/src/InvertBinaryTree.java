import java.util.ArrayList;

/*
    // Invert binary tree (AlgoExpert : Medium)
                    1                                        1
                 /     \                                  /      \
                2       3                                3        2
              /   \   /   \         ==>>               /   \    /   \
             4    5  6     7                          7     6  5     4
           /   \                                    /  \
          8     9                                  9    8
    #Idea!
    Do level-wise traversal and swap left and right child of each node.
 */
public class InvertBinaryTree {
    void invert (Node root) {
        ArrayList<Node> frontier = new ArrayList<>();
        frontier.add(root);
        while (!frontier.isEmpty()) {
            ArrayList<Node> next = new ArrayList<>();
            for (Node node : frontier) {
                Node temp = node.left;
                node.left = node.right;
                node.right = temp;
                if (node.left != null) next.add(node.left);
                if (node.right != null) next.add(node.right);
            }
            frontier = next;
        }
    }
}
