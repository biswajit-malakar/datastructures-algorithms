/*
    // Node Depths (AlgoExpert : Easy)
    // Get depth of all nodes from the root and sum all of them and return.
                                1
                            /       \
                           2         3
                        /    \    /    \
                       4      5  6      7
                     /   \
                    8     9
             Answer : 16
    #Idea!
    Do post-order travelling, pass depth(initially Zero) on parameter and return (left depth + right depth + current depth)
 */
public class NodeDepths {
    int depths (Node root, int depth) {
        if (root == null) return 0;
        int left = depths (root.left, depth+1);
        int right = depths (root.right, depth+1);
        return depth+left+right;
    }
}
