import java.util.ArrayList;

/*
    // Compare Leaf nodes (AlgoExpert : Very Hard)
    // Given two Binary Trees and compare their leaf nodes if they are same then return 'ture' otherwise 'false'.
                        Tree 1                                  Tree 2
                           1                                        1
                      /         \                              /          \
                     5           3                            2            4
                  /     \     /     \                     /       \     /     \
                '9'     '7' '8'      6                  '9'       5    6      '3'
                                    /                           /       \
                                  '3'                         '7'       '8'
             leaf nodes = [9,7,8,3]                  leaf nodes = [9,7,8,3]           ==> Ans: True
 */
public class CompareLeafNode {
    boolean result (Node root1, Node root2) {
        ArrayList<Integer> leafs1 = new ArrayList<>();
        ArrayList<Integer> leafs2 = new ArrayList<>();
        getLeafs (root1, leafs1); // get leaf nodes of tree1
        getLeafs (root2, leafs2); // get leaf nodes of tree2
        return leafs1.equals(leafs2); // return 'true' if equals and 'false' if not.
    }

    // Get leaf nodes of a tree and store them into a list.
    void getLeafs (Node root, ArrayList<Integer> leafs) {
        if (root == null) return;
        if (root.left==null && root.right==null) {
            leafs.add(root.item); return;
        }
        getLeafs(root.left, leafs);
        getLeafs(root.right, leafs);
    }
}
