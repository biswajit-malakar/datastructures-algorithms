// HARD
/*
    // Max path sum in binary tree (AlgoExpert : Hard)
    A path in a binary tree is a sequence of nodes where each pair of adjacent nodes in the sequence has
    an edge connecting them. A node can only appear in the sequence at most once. Note that the path does
    not need to pass through the root.
    The path sum of a path is the sum of the node's values in the path.
    Given the root of a binary tree, return the maximum path sum of any non-empty path.

    Input: root = [1,2,3]
    Output: 6
    Explanation: The optimal path is 2 -> 1 -> 3 with a path sum of 2 + 1 + 3 = 6.

    Input: root = [-10,9,20,null,null,15,7]
    Output: 42
    Explanation: The optimal path is 15 -> 20 -> 7 with a path sum of 15 + 20 + 7 = 42.

    #Idea!
    Create a global variable to store max path sum.
    Take pyramid shaped path sum and compare with global variable store the maximum one.
    Pass line shaped path sum to the upper level.
    If line shaped path sum is negative then do not take the line path return zero.
 */
public class MaxPathSum {
    int ans = Integer.MIN_VALUE;
    int max_path_sum (Node root) {
        max_path_sum_helper(root);
        return ans;
    }
    int max_path_sum_helper (Node root) {
        if (root == null) return 0;
        int left_sum = max_path_sum_helper(root.left);
        int right_sum = max_path_sum_helper(root.right);
        if (left_sum < 0) left_sum = 0;
        if (right_sum < 0) right_sum = 0;
        ans = Math.max(ans, (left_sum+root.item+right_sum));
        return root.item + Math.max(left_sum, right_sum);
    }
}
