/*
    // Binary tree diameter (AlgoExpert : Medium)
    // Diameter is the longest path from one node to another node.
                                1
                            /       \
                           3         2
                         /   \
                        7     4
                      /        \
                     8          5
                   /             \
                  9               6
                  Answer : 7 {9,8,7,3,4,5,6}
 */
public class BinaryTreeDiameter {
    int answer = Integer.MIN_VALUE;
    int diameter (Node root) {
        if (root == null) return 0;
        int left = diameter(root.left);
        int right = diameter(root.right);
        answer = Math.max(answer, (left+right+1));
        return Math.max(left, right) + 1;
    }

    int result (Node root) {
        diameter(root);
        return answer;
    }
}
