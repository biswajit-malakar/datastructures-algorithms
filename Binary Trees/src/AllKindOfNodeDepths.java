import java.util.HashMap;
import java.util.Set;

/*
    // All Kind of Node Depths (AlgoExpert : Very Hard)
                    1
                /       \
               2         3
            /    \    /    \
           4      5  6      7
         /  \
        8    9
        ANS : 26
    Calculate depth of all individual 'Node' based on all the subtrees that the 'Node' is part of.
    Sum the depths of all 'Nodes'.

    # Count nodes in a subtree #O(N)
    if (leaf node) count[node] = 1
    count[node] = count[node.left] + count[node.right] + 1
    Post-Order Traversal

    # Find sum of depths of a node for all possible subtrees #O(N)
    if (leaf node) count[node] = 0
    depth[node] = depth[node.left] + depth[node.right] + count[node.left] + count[node.right]
    Post-Order Traversal

    # Final Answer : sum all depths #O(N)
    sum = 0
    for (node : depth)
        sum += depth.get(node)
    return sum
 */
public class AllKindOfNodeDepths {
    // Straight forward algorithm
//    // helper function
//    int depth_of (Node node, int depth) { // find depth of all nodes in a subtree. Up to Bottom approach
//        if (node == null) return depth;
//        return depth_of(node.left, depth+1) + depth_of(node.right, depth+1) + depth;
//    }
//
//    // Main function to call
//    int all_depths (Node node) { // find depths of all nodes in all possible subtree.
//        if (node == null) return 0;
//        return all_depths(node.left) + all_depths(node.right) + depth_of(node, 0);
//    }

    // Optimal Algorithm
    int result (Node root) {
        HashMap<Node, Integer> count = new HashMap<>();
        HashMap<Node, Integer> depth = new HashMap<>();
        getCounts (root, count);    // get count
        getDepths (root, depth, count);     // get depths

        // sum all the depths and return
        int sum = 0;
        Set<Node> nodes = depth.keySet();
        for (Node node : nodes )
            sum += depth.get(node);
        return sum;
    }

    // get count of nodes for all subtrees and store them in count hashmap
    void getCounts (Node root, HashMap<Node, Integer> count) {
        if (root == null) return;
        if (root.left==null && root.right==null) count.put(root, 1);
        getCounts(root.left, count);
        getCounts(root.right, count);
        int sum = count.get(root.left) + count.get(root.right) + 1;
        count.put(root, sum);
    }

    // get sum of depths for all nodes for all subtrees in depth hashmap
    void getDepths (Node root, HashMap<Node,Integer> depth, HashMap<Node,Integer> count) {
        if (root == null) return;
        if (root.left==null && root.right==null) depth.put(root, 0);
        getDepths(root.left, depth, count);
        getDepths(root.right, depth, count);
        int sum = depth.get(root.left) + depth.get(root.right) + count.get(root.left) + count.get(root.right);
        depth.put(root, sum);
    }
}