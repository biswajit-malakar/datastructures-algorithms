import java.util.ArrayList;

/*
    // Branch sums (AlgoExpert : Easy)
    // Branch is a path from root to a leaf node
                        1
                     /     \
                    2       3
                 /   \    /   \
                4     5  6     7
              /  \   /
             8    9 10
         Answer : [15, 16, 18, 10, 11]
 */
public class BranchSums {
    void branch_sums (Node root, ArrayList<Integer> list, int sum) {
        if (root == null) return;
        if (root.left==null && root.right==null) {
            list.add(sum+root.item);
            return;
        }
        branch_sums(root.left, list, sum+root.item);
        branch_sums(root.right, list, sum+root.item);
    }

    ArrayList<Integer> result (Node root) {
        ArrayList<Integer> list = new ArrayList<>();
        branch_sums(root, list, 0);
        return list;
    }
}
