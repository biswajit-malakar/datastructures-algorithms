// THIS IS A BASIC NODE CLASS TO CREATE NODE FOR OTHER CLASSES.
public class Node {
    int item;
    Node left;
    Node right;
    Node parent;
    Node (int item) {
        this.item = item;
        left = null;
        right = null;
        parent = null;
    }

    Node () {

    }
}
