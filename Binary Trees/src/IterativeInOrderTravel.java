import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
/*
    // Iterative In-Order traversal (AlgoExpert : Very Hard)
    // Given a tree do in-order traversal using O(1) space or using iteration.
 */
public class IterativeInOrderTravel {
    // NOT THAT BAD IMPLEMENTATION
    void inorder_traversal (Node A) {
        HashMap<Node, Node> visited = new HashMap<>();
        inorder_traversal_helper(A, visited);
    }

    void inorder_traversal_helper (Node A, HashMap<Node, Node> visited) {
        while (A != null) {
            if (visited.containsKey(A)) A = A.parent;
            else if (A.left!=null && !visited.containsKey(A.left)) A = A.left;
            else {
                System.out.print(A.item+", ");
                visited.put (A, A.parent);
                if (A.right != null) A = A.right;
                else A = A.parent;
            }
        }
    }

    // MUCH BETTER IMPLEMENTATION
    public List<Integer> inorderTraversal(Node root) {
        List<Integer> list = new ArrayList<>();
        Stack<Node> stack = new Stack<>();
        while (root!=null || !stack.isEmpty()) {
            if (root != null) {
                stack.push (root);
                root = root.left;
            }
            else {
                root = stack.pop();
                list.add (root.item);
                root = root.right;
            }
        }
        return list;
    }
}
