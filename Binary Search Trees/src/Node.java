public class Node {
    int item;
    Node left;
    Node right;
    Node (int item) {
        this.item = item;
        this.left = null;
        this.right = null;
    }
    Node () {}
}
