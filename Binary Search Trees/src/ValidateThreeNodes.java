/*
    // Validate three nodes (AlgoExpert : Hard)
    // Given three nodes nodeOne, nodeTwo, nodeThree. Your task is to find out is nodeTwo lines in between
    nodeThree and nodeOne.
                                5
                            /       \
                           2         7
                        /     \    /   \
                       1       4  6     8
                     /        /
                    0        3
    #Example 1: nodeOne = 5, nodeTwo = 2, nodeThree = 3 (True)
    #Example 2: nodeOne = 5, nodeTwo = 8, nodeThree = 3 (False)
    #Example 3: nodeOne = 0, nodeTwo = 1, nodeThree = 5 (True)

    Idea #1::
    1. Check which one of nodeOne or nodeThree is descendant of nodeTwo. Do this via looking for that node from nodeTwo.
    2. Check for other one is ancestor of nodeTwo. Do this via looking for nodeTwo.

    Idea #2::
    Start from any one direction at a time. Maybe from nodeOne or nodeThree looking for other node. While looking for other node
    if we found nodeTwo it means starting node is ancestor of nodeTwo (solved half problem) now all we have to do is to find that
    the other node is descendant of nodeTwo.
 */
public class ValidateThreeNodes {
    // Idea #1  Time O(h) | Space O(h)
//    boolean validate (Node nodeOne, Node nodeTwo, Node nodeThree) {
//        if (search(nodeTwo, nodeOne)) // if nodeOne is descendant of nodeTwo
//            return search(nodeThree, nodeTwo); // if nodeThree is ancestor of nodeTwo
//        else if (search(nodeTwo, nodeThree)) // if nodeThree is descendant of nodeTwo
//            return search(nodeOne, nodeTwo); // if nodeOne is ancestor of nodeTwo
//        else return false;
//    }
//
//    boolean search (Node node, Node target) {
//        if (node == null) return false;
//        if (node.item == target.item) return true;
//        else if (target.item < node.item) return search(node.left, target);
//        else return search(node.right, target);
//    }



    // Idea #1  Time O(h) | Space O(1)
//    boolean validate (Node nodeOne, Node nodeTwo, Node nodeThree) {
//        if (search(nodeTwo, nodeOne)) // if nodeOne is descendant of nodeTwo
//            return search(nodeThree, nodeTwo); // if nodeThree is ancestor of nodeTwo
//        else if (search(nodeTwo, nodeThree)) // if nodeThree is descendant of nodeTwo
//            return search(nodeOne, nodeTwo); // if nodeOne is ancestor of nodeTwo
//        else return false;
//    }
//
//    boolean search (Node node, Node target) {
//        while (node!=null) {
//            if (node.item == target.item) return true;
//            else if (target.item < node.item) node = node.left;
//            else node = node.right;
//        }
//        return false;
//    }



    // Idea #2  Time O(d) | Space O(d)
//    boolean searchFromNodeTwo (Node nodeTwo, Node target) {
//        if (nodeTwo == null) return false;
//        if (nodeTwo.item == target.item) return true;
//        else if (target.item < nodeTwo.item) return searchFromNodeTwo(nodeTwo.left, target);
//        else return searchFromNodeTwo(nodeTwo.right, target);
//    }
//
//    boolean searchForOtherNode (Node nodeOne, Node nodeTwo, Node nodeThree) {
//        if (nodeOne == null) return false;
//        if (nodeOne.item == nodeThree.item) return false;
//        if (nodeOne.item == nodeTwo.item) return searchFromNodeTwo(nodeTwo, nodeThree);
//        if (nodeThree.item < nodeOne.item) return searchForOtherNode(nodeOne.left, nodeTwo, nodeThree);
//        else return searchForOtherNode(nodeOne.right, nodeTwo, nodeThree);
//    }
//
//    boolean validate (Node nodeOne, Node nodeTwo, Node nodeThree) {
//        boolean direction1 = searchForOtherNode(nodeOne, nodeTwo, nodeThree);
//        boolean direction2 = false;
//        if (!direction1)
//            direction2 = searchForOtherNode(nodeThree, nodeTwo, nodeOne);
//        return direction1 || direction2;
//    }



    // Idea #2  Time O(d) | Space O(1)
    boolean searchFromNodeTwo (Node nodeTwo, Node target) {
        while (nodeTwo != null) {
            if (nodeTwo.item == target.item) return true;
            else if (target.item < nodeTwo.item) nodeTwo = nodeTwo.left;
            else nodeTwo = nodeTwo.right;
        }
        return false;
    }

    boolean searchForOtherNode (Node nodeOne, Node nodeTwo, Node nodeThree) {
        while (nodeOne != null) {   // keep searching until nodeOne is null.
            if (nodeOne.item == nodeThree.item) return false;       // did we find end point of the path ?
            if (nodeOne.item == nodeTwo.item)   // did we find nodeTwo ?
                return searchFromNodeTwo(nodeTwo, nodeThree);   // nodeOne is ancestor of nodeTwo, all we have to find is nodeThree is descendant of nodeTwo.
            if (nodeThree.item < nodeOne.item) nodeOne = nodeOne.left;  // going to a direction based on nodeThree.item
            else nodeOne = nodeOne.right;
        }
        return false;   // if nodeOne is null meaning we did not find any of others
    }

    boolean validate (Node nodeOne, Node nodeTwo, Node nodeThree) {
        boolean direction1 = searchForOtherNode(nodeOne, nodeTwo, nodeThree); // path from nodeOne to nodeThree
        boolean direction2 = false;
        if (!direction1)
            direction2 = searchForOtherNode(nodeThree, nodeTwo, nodeOne); // path from nodeThree to nodeOne
        return direction1 || direction2;
    }

}
