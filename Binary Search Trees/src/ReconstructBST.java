/*
    // Reconstruct BST (AlgoExpert : Medium)
    // Given an array created doing pre-order traversal on a BST. Task is to create a new BST using the given
    array using pre-order traversal.
                    [ 10, 4, 2, 1, 5, 17, 19, 18 ]
                                10
                            /         \
                           4          17
                        /     \          \
                       2      5          19
                     /                 /
                    1                 18
    #Idea!
    Do pre-order traversal each time check is the next element from array do it falls in the range!
 */
public class ReconstructBST {
    Node result (int[] array) {
        if (array.length < 1) return null;
        return helper (array, Long.MIN_VALUE, Long.MAX_VALUE, null);
    }

    int i = 0;
    Node helper (int[] array, long min, long max, Node root) {
        if (i >= array.length) return root;
        if (root == null) root = new Node(array[i]);
        else {
            if (min<=array[i] && array[i]<max) {
                if (array[i] < root.item) {
                    root.left = new Node(array[i]);
                    root = root.left;
                }
                else {
                    root.right = new Node(array[i]);
                    root = root.right;
                }
            }
            else return root;
        }
        i++;
        helper(array, min, root.item, root);
        helper(array, root.item, max, root);
        return root;
    }
}
