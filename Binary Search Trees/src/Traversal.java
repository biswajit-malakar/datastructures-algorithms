import java.util.ArrayList;

/*
    // Travel over a BST. (AlgoExpert : Medium)
    // Do In-Order, Pre-Order, Post-Order traversal
                        10
                    /        \
                   5         15
                /    \          \
               2      5         22
              /
             1

    In-Order : [1, 2, 5, 5, 10, 15, 22]
    Pre-Order : [10, 5, 2, 1, 5, 15, 22]
    Post-Order : [1, 2, 5, 4, 22, 15, 10]
 */
public class Traversal {

    // Recursive solutions
    ArrayList<Integer> in_order_recursive (Node root) {
        ArrayList<Integer> list = new ArrayList<>();
        if (root == null) return list;
        list.addAll(in_order_recursive(root.left));
        list.add(root.item);
        list.addAll(in_order_recursive(root.right));
        return list;
    }

    ArrayList<Integer> pre_order_recursive (Node root) {
        ArrayList<Integer> list = new ArrayList<>();
        if (root == null) return list;
        list.add(root.item);
        list.addAll(in_order_recursive(root.left));
        list.addAll(in_order_recursive(root.right));
        return list;
    }

    ArrayList<Integer> post_order_recursive (Node root) {
        ArrayList<Integer> list = new ArrayList<>();
        if (root == null) return list;
        list.addAll(in_order_recursive(root.left));
        list.addAll(in_order_recursive(root.right));
        list.add(root.item);
        return list;
    }

}
