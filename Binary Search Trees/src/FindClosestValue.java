/*
    // Find the closest value in BST (AlgoExpert : Easy)
    // Given a BST and an Integer number, your task is to find the nearest value of given number from BST.
                            10
                       /         \
                      5           15
                  /      \      /     \
                 2        5    13      22
              /                  \
             1                    14
             Answer : Closest value of 12 is 13 present in BST.
 */
public class FindClosestValue {
    int result (Node root, int k) {
        return helper (root, k, root.item);
    }

    int helper (Node root, int k, int closest) {
        if (root == null) return closest;

        if (Math.abs(closest-k) > Math.abs(root.item-k)) closest = root.item;

        if (k < root.item) return helper(root.left, k, closest);
        else if (k > root.item) return helper(root.right, k, closest);
        else return closest;
    }
}
