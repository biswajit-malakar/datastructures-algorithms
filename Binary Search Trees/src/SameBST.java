import java.util.ArrayList;

/*
    // Same BSTs (AlgoExpert : Hard)
    // Given two Integer arrays your task is to check are those two arrays representing same BST or not.

    Array1 = {10, 15, 8, 12, 94, 81, 5, 2, 11}
    Array2 = {10, 8, 5, 15, 2, 12, 11, 94, 81}
    Answer : True

    #Idea!
    check #1 : len(Array1) and len(Array2) are same or not
    Recursive :
        check #2 : check for root (first element from arrays)
        Separate all elements lies at the left and right of the root.
 */
public class SameBST {

    // Time O(n^2) | Space O(n^2)
//    boolean sameBstOrNot (ArrayList<Integer> arrayOne, ArrayList<Integer> arrayTwo) {
//        // Check #1 :If two arrays are not same length then stop and return false
//        if (arrayOne.size() != arrayTwo.size())
//            return false;
//        // If two arrays are same then return true
//        if (arrayOne.size()==0 && arrayTwo.size()==0)
//            return true;
//        // Check #2 :If root[first element] of both arrays are not same then stop and return false
//        if (arrayOne.get(0) != arrayTwo.get(0))
//            return false;
//        // Separate smaller and larger values from arrayOne and arrayTwo
//        ArrayList<Integer> leftOne = getSmaller(arrayOne);
//        ArrayList<Integer> leftTwo = getSmaller(arrayTwo);
//        ArrayList<Integer> rightOne = getLargerOrEqual(arrayOne);
//        ArrayList<Integer> rightTwo = getLargerOrEqual(arrayTwo);
//        // recursively check for left and right arrays...
//        return sameBstOrNot(leftOne, leftTwo) && sameBstOrNot(rightOne, rightTwo);
//    }
//
//    ArrayList<Integer> getSmaller (ArrayList<Integer> array) {
//        ArrayList<Integer> smaller = new ArrayList<>();
//        for (int i=1; i<array.size(); i++) {
//            if (array.get(i) < array.get(0))
//                smaller.add(array.get(i));
//        }
//        return smaller;
//    }
//
//    ArrayList<Integer> getLargerOrEqual (ArrayList<Integer> array) {
//        ArrayList<Integer> largerOrEqual = new ArrayList<>();
//        for (int i=1; i<array.size(); i++) {
//            if (array.get(i) >= array.get(0))
//                largerOrEqual.add(array.get(i));
//        }
//        return largerOrEqual;
//    }




    // Time O(n^2) | Space O(h)
    // Everything is quite same but passing indices rather creating new arrays in each call. Also keeping track of possible value range for root node.
    boolean sameBstOrNot (ArrayList<Integer> arrayOne, ArrayList<Integer> arrayTwo) {
        return areSameBst (arrayOne, arrayTwo, 0, 0, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    boolean areSameBst (ArrayList<Integer> arrayOne, ArrayList<Integer> arrayTwo, int rootIdxOne, int rootIdxTwo, long minValue, long maxValue) {
        if (rootIdxOne==-1 || rootIdxTwo==-1)
            return rootIdxOne == rootIdxTwo;

        if (arrayOne.get(rootIdxOne) != arrayTwo.get(rootIdxTwo))
            return false;

        int leftRootIdxOne = getIdxOfFirstSmaller(arrayOne, rootIdxOne, minValue);
        int leftRootIdxTwo = getIdxOfFirstSmaller(arrayTwo, rootIdxTwo, minValue);
        int rightRootIdxOne = getIdxOfFirstBiggerOrEqual(arrayOne, rootIdxOne, maxValue);
        int rightRootIdxTwo = getIdxOfFirstBiggerOrEqual(arrayTwo, rootIdxTwo, maxValue);

        long currentValue = arrayOne.get(rootIdxOne);
        boolean leftAreSame = areSameBst(arrayOne, arrayTwo, leftRootIdxOne, leftRootIdxTwo, minValue, currentValue);
        boolean rightAreSame = areSameBst(arrayOne, arrayTwo, rightRootIdxOne, rightRootIdxTwo, currentValue, maxValue);

        return leftAreSame && rightAreSame;
    }

    int getIdxOfFirstSmaller (ArrayList<Integer> array, int startingIdx, long minValue) {
        for (int i=startingIdx+1; i<array.size(); i++) {
            if (array.get(i)<array.get(startingIdx) && array.get(i)>=minValue)
                return i;
        }
        return -1;
    }

    int getIdxOfFirstBiggerOrEqual (ArrayList<Integer> array, int startingIdx, long maxValue) {
        for (int i=startingIdx+1; i<array.size(); i++) {
            if (array.get(i)>=array.get(startingIdx) && array.get(i)<maxValue)
                return i;
        }
        return -1;
    }
}
