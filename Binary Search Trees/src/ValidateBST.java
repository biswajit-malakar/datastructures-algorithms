/*
    // Validate a given Binary Search Tree (AlgoExpert : Medium)
    // Check is the Binary Tree satisfy the BST property or not.

    A valid BST is defined as follows:
    The left subtree of a node contains only nodes with keys less than the node's key.
    The right subtree of a node contains only nodes with keys greater than the node's key.
    Both the left and right subtrees must also be binary search trees.

    #Idea!
    Every 'node' has a range find that range and do post-order traversal.
 */

// Will Try Again Tomorrow!
public class ValidateBST {
    boolean check (Node root) {
        return validate (root, Float.MIN_VALUE, Float.MAX_VALUE);
    }

    boolean validate (Node node, float min, float max) {
        if (node == null) return true;
        if (node.item>=max || node.item<=min) return false;
        return validate(node.left, min, node.item) && validate(node.right, node.item, max);
    }
}
