import java.util.HashMap;

/*
    // Construction of a binary tree (AlgoExpert : Medium)
                            10
                       /         \
                      5           15
                  /      \      /     \
                 2        5    13      22
              /                  \
             1                    14
 */
public class BSTConstruction {

    // Insert into an item into a Binary Search Tree
    void insertion (Node root, int item) {
        if (root == null) return;
        if (item < root.item) {
            if (root.left != null) insertion(root.left, item);
            else root.left = new Node(item);
        }
        else {
            if (root.right != null) insertion(root.right, item);
            else root.right = new Node(item);
        }
    }

    // Search for a target on Binary Search Tree
    boolean searching (Node root, int target) {
        if (root == null) return false;
        if (target == root.item) return true;
        else if (target < root.item) return searching(root.left, target);
        else return searching(root.right, target);
    }


    // Delete a node having 'key' from a Binary Search Tree
    Node deleteNode(Node root, int key) {
        if (root.item==key && root.left==null && root.right==null) { root = null; return null;}
        HashMap<Node, Node> parent = new HashMap<>();
        parent.put(root, null);
        augmenting (root, parent);
        delete (root, key, parent);
        return root;
    }

    void augmenting (Node root, HashMap<Node,Node> parent) {
        if ((root==null) || (root.left==null&&root.right==null)) return;
        if (root.left!=null) parent.put(root.left, root);
        if (root.right!=null) parent.put(root.right, root);
        augmenting(root.left, parent);
        augmenting(root.right, parent);
    }

    Node delete (Node A, int key, HashMap<Node,Node> parent) {
        if (A == null) return null;
        if (key < A.item) return delete (A.left, key, parent);
        else if (key > A.item) return delete (A.right, key, parent);
        else {
            if (A.left==null && A.right==null) {
                Node B = A;
                if (parent.get(A) == null) A = null;
                if (parent.get(A).left == A) parent.get(A).left = null;
                else parent.get(A).right = null;
                return B;
            }
            else {
                Node B;
                if (A.left != null) B = last_node (A.left);
                else B = first_node (A.right);
                int temp = A.item;
                A.item = B.item;
                B.item = temp;
                return delete (B, key, parent);
            }
        }
    }

    Node last_node (Node A) {
        while (A.right != null) A = A.right;
        return A;
    }

    Node first_node (Node A) {
        while (A.left != null) A = A.left;
        return A;
    }
}
