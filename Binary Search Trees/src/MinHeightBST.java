/*
    // Min height BST (AlgoExpert : Medium)
    // Given a sorted, distinct array and insert() function. Task is to create a BST out of the array with
    minimum height possible.
            [1, 2, 5, 7, 10, 13, 14, 15, 22]
                        10
                    /        \
                   2         14
                /    \     /    \
               1      5   13    15
                        \         \
                         7        22

    #Idea!
    Sorted array and BST both are good at doing Binary Search. The key point in this problem is picking
    an element. If we run Binary Search on the array using recursion and pick element via this. Then rest
    of the work would be very easy.
    While inserting into BST we are using BST property to find out the direction where the element needs
    to be inserted in O(1) time.
 */
public class MinHeightBST {
    // This function will be given...
    Node insert (Node root, int item) {
        if (root == null) {
            root = new Node(item);
            return root;
        }
        if (item < root.item) {
            if (root.left != null) insert(root.left, item);
            root.left = new Node(item);
        }
        else {
            if (root.right != null) insert(root.right, item);
            root.right = new Node(item);
        }
        return root;
    }


    // Straight forward Solution
    // Time O(NlogN) | O(N) space
//    Node build (int[] array, Node root) {
//        return binary_search (array, 0, array.length-1, root);
//    }
//
//    Node binary_search (int[] array, int min, int max, Node root) {
//        if (max < min) return root;
//        int mid = (max+min) /2;
//        if (root == null) root = new Node(array[mid]);
//        insert(root, array[mid]);
//        binary_search(array, min, mid-1, root);
//        binary_search(array, mid+1, max, root);
//        return root;
//    }


    // Optimal Solution #1
//    Node build (int[] array, Node root) {
//        binary_search (array, 0, array.length-1, root, null, false);
//        return root;
//    }
//
//    void binary_search (int[] array, int min, int max, Node root, Node parent, boolean createAtLeft) {
//        if (max < min) return;
//        int mid = (max+min) /2;
//        if (root == null) root = new Node(array[mid]);
//        else if (createAtLeft) parent.left = new Node(array[mid]);
//        else parent.right = new Node(array[mid]);
//        binary_search(array, min, mid-1, root.left, root, true);
//        binary_search(array, mid+1, max, root.right, root, false);
//    }


    // Optimal Solution #2
    Node build (int[] array, Node root) {
        return binary_search (array, 0, array.length-1, root);
    }

    Node binary_search (int[] array, int min, int max, Node root) {
        if (max < min) return root;
        int mid = (max+min) /2;
        if (root == null) root = new Node(array[mid]);
        else {
            if (array[mid] < root.item) {
                root.left = new Node(array[mid]);
                root = root.left;
            }
            else {
                root.right = new Node(array[mid]);
                root = root.right;
            }
        }
        binary_search(array, min, mid-1, root);
        binary_search(array, mid+1, max, root);
        return root;
    }
}
