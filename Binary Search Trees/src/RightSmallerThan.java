/*
    // Right Smaller Than (AlgoExpert : Very Hard)
    // Given an array as input your task is to return another array of same size as input array.
    Each index of output array represents the number of elements that are strictly smaller than element of
    that index of input array and present at right side of that index.
    Input Array : [8, 5, 11, -1, 3, 4, 2]
    Output Array : [5, 4, 4, 0, 1, 1, 0]
 */
public class RightSmallerThan {
    // Straight forward solution    Time O(N^2) | Space O(N)
//    int[] solution (int[] array) {
//        int[] ans = new int[array.length];
//        int k = 0;
//        for (int i=0; i<array.length; i++) {    // iterate over all the elements of input array
//            int count = 0;  // initially # of smaller elements is Zero
//            for (int j=i+1; j<array.length; j++) {  // iterate over all the elements that are at right side
//                if (array[j] < array[i])    // if element is less than current element
//                    count++;    // increment the count
//            }
//            ans[k++] = count;   // at the last store the count
//        }
//        return ans;
//    }



    // Optimal Algorithm : Time O(N*h) | Space O(N)
    int[] rightSmallerThan (int[] array) {
        if (array.length == 0)
            return array;
        int[] rightSmallerThanCounts = new int[array.length];   // creating a new array for storing answers
        int lastIdx = array.length-1;
        rightSmallerThanCounts[lastIdx] = 0;    // right most element has 0 right smaller counts
        SpecialBST root = new SpecialBST(array[lastIdx]);   // creating root from last element
        for (int i=lastIdx-1; i>=0; i--)    // pick element from right to left
            insert (root, array[i], i, rightSmallerThanCounts, 0);  // insert element
        return rightSmallerThanCounts;  // return the array having counts right smaller than
    }

    void insert (SpecialBST root, int value, int idx, int[] rightSmallerThanCounts, int numSmallerAtInsertTime) {
        // root : root of the BST that we are creating
        // value : value that we want to store into the BST
        // idx : index of the value
        // rightSmallerThanCounts : array for storing answer, count of right smaller than value
        // numSmallerAtInsertTime : keep tracks while travelling how many smaller values you have visited
        if (value < root.item) {    // if value is less than root's item go to left
            root.leftSubtreeSize += 1;  // because you are going to create a new node at left side it means left subtree size for root is going to be increased
            if (root.left == null) {    // if root's left is null
                root.left = new SpecialBST(value);  // insert at left side of the root
                rightSmallerThanCounts[idx] = numSmallerAtInsertTime;   // after inserting update rightSmallerThanCounts by number of values you have visited which is less (tracks by numSmallerAtInsertTime)
            }
            else insert(root.left, value, idx, rightSmallerThanCounts, numSmallerAtInsertTime); // if there is more node at left then go to left
        }
        else {
            numSmallerAtInsertTime += root.leftSubtreeSize;     // because you are going to right update number of values you have visited less than provided value
            if (value > root.item)  // if root's item is less than value provided
                numSmallerAtInsertTime += 1;    // consider counting root as node has value less than value provided
            if (root.right == null) {   // if there is no more node at right
                root.right = new SpecialBST(value); // then add a new node at right
                rightSmallerThanCounts[idx] = numSmallerAtInsertTime;   // after adding a new node update answer (number of nodes you have visited which is less than you)
            }
            else insert(root.right, value, idx, rightSmallerThanCounts, numSmallerAtInsertTime);    // if there is more nodes at the right go to the right
        }
    }

    // class for node
    class SpecialBST {
        int item;
        SpecialBST left, right;
        int leftSubtreeSize;    // augmentation, for storing number of nodes that are at left side.
        SpecialBST (int item) {
            this.item = item;
            left = null;
            right = null;
            leftSubtreeSize = 0;    // initially, for a new node number of nodes at left side is 0
        }
    }
}
