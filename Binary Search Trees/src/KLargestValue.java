import java.util.ArrayList;

/*
    // Find kth largest value in BST (AlgoExpert : Medium)
    // Given a BST and an integer number 'k'. Task is to find kth largest value from the BST.
                        15
                    /        \
                   5         20
                /    \     /    \
               2      5   17    22
             /  \
            1    3
           K = 3
           Answer : 17
 */
public class KLargestValue {

    // O(N) Time | O(N) Space
//    int result (Node root, int k) {
//        ArrayList<Integer> list = new ArrayList<>();
//        in_order_traversal (root, list);
//        return list.get(list.size()-k);
//    }
//
//    void in_order_traversal (Node root, ArrayList<Integer> list) {
//        if (root == null) return;
//        in_order_traversal(root.left, list);
//        list.add(root.item);
//        in_order_traversal(root.right, list);
//    }


    // O(logN) Time | O(logN) Space
    int visited_counts = 0;
    int ans;
    int result (Node root, int k) {
        reverse_in_order_traversal(root, k);
        return ans;
    }
    void reverse_in_order_traversal (Node root, int k) {
        if (root == null) return;
        reverse_in_order_traversal(root.right, k);
        visited_counts++;
        if (k == visited_counts) ans = root.item;
        reverse_in_order_traversal(root.left, k);
    }
}
