public class Basics {
    public static void main(String[] args) {

    }
}

class Node {
    int item;
    Node parent;
    Node left;
    Node right;
    Node (int item) {
        this.item = item;
    }
}



class Tree_Operations {
    Node subtree_first (Node A) {
        if (A.left == null) return A;
        return subtree_first(A.left);
    }

    Node subtree_last (Node A) {
        if (A.right == null) return A;
        return subtree_last(A.right);
    }

    Node subtree_find (Node A, int item) {
        if (A == null) return A;
        if (A.item > item) return subtree_find(A.left, item);
        else if (A.item == item) return A;
        else return subtree_find(A.right, item);
    }

    void subtree_insert (Node A, int item) {
        if (A == null) A = new Node(item);
        if (item < A.item) {
            if (A.left == null) A.left = new Node (item);
            else subtree_insert(A.left, item);
        }
        else {
            if (A.right == null) A.right = new Node (item);
            else subtree_insert(A.right, item);
        }
    }

    void subtree_delete (Node A) {
        if (A.left==null && A.right==null) {
            if (A.parent == null) A = null;
            else {
                if (A.parent.left == A) A.parent.left = null;
                else A.parent.right = null;
            }
        }
        else {
            Node B;
            if (A.left != null) B = subtree_last (A);
            else B = subtree_first (A);
            int temp = A.item;
            A.item = B.item;
            B.item = temp;
            subtree_delete(B);
        }
    }

    Node subtree_build (int[] A) {
        Node root = new Node (A[0]);
        for (int i=0; i<A.length; i++) subtree_insert(root, A[i]);
        return root;
    }
}
