import java.util.*;
class Basics {
    public static void main(String[] args) {
        int[] A = {3,6,1,2,0,5,2,7,9,1};
        Heap_Operations operations = new Heap_Operations();
        //Heap_Construction heap = new Heap_Construction(10);
        operations.inplace_heap_sort(A);
        System.out.println (Arrays.toString (A));
    }
}

class Heap_Construction {
    int[] array;
    Heap_Construction (int size) {
        array = new int[size];
    }
}


class Heap_Operations {
    static int N=0;
    // get parent node index of 'c' #O(1)
    int parent(int c) {
        int p = (c-1) / 2;
        if (p < 0) return c;
        return p;
    }

    // get left child node index of 'p' #O(1)
    int left (int p) {
        int c = (2*p) + 1;
        if (c >= N) return p;
        return c;
    }

    // get right child node index of 'p' #O(1)
    int right (int p) {
        int c = (2*p) + 2;
        if (c >= N) return p;
        return c;
    }

    // restore the max heap property violated by child 'c' #O(log N)
    void max_heapify_up (int[] A, int c) {
        int p = parent (c);
        if (A[p] < A[c]) {
            int temp = A[p];
            A[p] = A[c];
            A[c] = temp;
            max_heapify_up (A, p);
        }
    }

    // restore max heap property violated by parent 'p' #O(log N)
    void max_heapify_down (int[] A, int p) {
        int c = left (p);
        if (A[c] < A[right (p)]) c = right (p);
        if (A[p] < A[c]) {
            int temp = A[p];
            A[p] = A[c];
            A[c] = temp;
            max_heapify_down (A, c);
        }
    }

    // insert an item into the heap #O(log N)
    void insert (int[] A, int item) {
        A[N] = item;
        N++;
        max_heapify_up (A, N-1);
    }

    // delete maximum element from heap #O(log N)
    int delete_max (int[] A) {
        int temp = A[0];
        A[0] = A[N-1];
        A[N-1] = temp;
        N--;
        max_heapify_down (A, 0);
        return A[N];
    }

    // inplace heap sort algorithm insert into delete and delete from heap. #O(N log N)
    void inplace_heap_sort (int[] A) {
        for (int i=0; i<A.length; i++) insert (A, A[i]);
        for (int i=A.length-1; i>=0; i--) A[i] = delete_max (A);
    }
}
