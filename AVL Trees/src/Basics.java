public class Basics {
    public static void main(String[] args) {
        int[] A = {3,5,1,9,0,3,4,1,8,7};
        Tree_Operations o = new Tree_Operations();
        Node Root = o.build(A);
        o.inorder_traversal(Root);
    }
}


class Node {
    int item;
    Node parent;
    Node left;
    Node right;
    int height;
    Node (int item) {
        this.item = item;
        this.height = 0;
        this.left = null;
        this.right = null;
        this.parent = null;
    }
}

class Tree_Operations {
    void inorder_traversal (Node A) {
        if (A == null) return;
        inorder_traversal(A.left);
        System.out.print("-> " + A.item);
        inorder_traversal(A.right);
    }

    Node subtree_first (Node A) {
        if (A.left == null) return A;
        return subtree_first(A.left);
    }

    Node subtree_last (Node A) {
        if (A.right == null) return A;
        return subtree_last(A.right);
    }

    Node successor (Node A) {
        if (A.right != null) return subtree_first(A.right);
        while (A.parent!=null && A.parent.right==A) A = A.parent;
        return A.parent;
    }

    Node predecessor (Node A) {
        if (A.left != null) return subtree_last(A.left);
        while (A.parent!=null && A.parent.left==A) A = A.parent;
        return A.parent;
    }

    void subtree_insert_before (Node A, Node X ) {
        if (A.left == null) {
            A.left = X;
            X.parent = A;
        }
        else {
            A = subtree_last(A.left);
            A.right = X;
            X.parent = A;
        }
        maintain(A);
    }

    void subtree_insert_after (Node A, Node X) {
        if (A.right == null) {
            A.right = X;
            X.parent = A;
        }
        else {
            A = subtree_first(A.right);
            A.left = X;
            X.parent = A;
        }
        maintain(A);
    }

    void subtree_insert (Node A, Node X) {
        if (A.item > X.item) {
            if (A.left == null) {
                A.left = X;
                X.parent = A;
            }
            else {
                subtree_insert (A.left, X);
                return;
            }
        }
        else {
            if (A.right == null) {
                A.right = X;
                X.parent = A;
            }
            else {
                subtree_insert (A.right, X);
                return;
            }
        }
        maintain (A);
    }

    void subtree_delete (Node A) {
        if (A.left==null && A.right==null) {
            if (A.parent == null) A = null;
            else {
                if (A.parent.left == A) A.parent.left = null;
                else A.parent.right = null;
                maintain(A.parent);
            }
        }
        else {
            Node B;
            if (A.left != null) B = subtree_last(A.left);
            else B = subtree_first(A.right);
            int temp = A.item;
            A.item = B.item;
            B.item = temp;
            subtree_delete(B);
        }
    }

    int height (Node A) {
        if (A == null) return -1;
        return A.height;
    }

    int skew (Node A) {
        return height (A.right) - height (A.left);
    }

    void subtree_rotate_right (Node D) {
        /*
         *          <D>                <B>
         *      <B>     <E>   ->>    <A>     <D>
         *    <A> <C>                     <C>  <E>
         */
        if (D.left == null) return;
        Node B = D.left, E = D.left;
        Node A = B.left, C = B.right;

        Node temp_node = D;
        D = B;
        B = temp_node;

        int temp_item = D.item;
        D.item = B.item;
        B.item = temp_item;

        D.left = C;
        D.right = E;
        B.left = A;
        B.right = D;

        if (A != null) A.parent = B;
        if (C != null) C.parent = D;
        if (E != null) E.parent = D;

        subtree_update (D);
        subtree_update (B);
    }

    void subtree_rotate_left (Node B) {
        /*
         *          <B>                         <D>
         *      <A>     <D>        ->>      <B>     <E>
         *            <C> <E>             <A> <C>
         */
        if (B.right == null) return;
        Node D = B.right, A = B.left;
        Node C = D.left, E = D.right;

        Node temp_node = D;
        D = B;
        B = temp_node;

        int temp_data = D.item;
        D.item = B.item;
        B.item = temp_data;

        B.left = A;
        B.right = C;
        D.left = B;
        D.right = E;

        if (A != null) A.parent = B;
        if (C != null) C.parent = B;
        if (E != null) E.parent = D;

        subtree_update (B);
        subtree_update (D);
    }

    void subtree_update (Node A) {
        A.height = Math.max(height(A.left), height(A.right)) + 1;
    }

    void rebalance (Node A) {
        if (skew(A) == 2) { // 'A' is right heavier
            if (skew(A.right) < 0) // 'A.right' is left heavy
                subtree_rotate_right(A.right);
            subtree_rotate_left(A);
        }
        if (skew(A) == -2) { // 'A' is left heavier
            if (skew(A.left) > 0) // 'A.left' is right heavy
                subtree_rotate_left(A.left);
            subtree_rotate_right(A);
        }
    }

    void maintain (Node A) {
        rebalance(A);
        subtree_update(A);
        if (A.parent != null)
            maintain(A.parent);
    }

    Node build (int[] A) {
        Node Root = new Node (A[0]);
        for (int i=1; i<A.length; i++) {
            Node new_node = new Node(A[i]);
            subtree_insert(Root, new_node);
        }
        return Root;
    }
}

